//Inheritance in Swift 4

import UIKit

class Animal { //final class or property cannot inherite, so don't final or val for those to be inherited
    var name: String = ""
    func eat() {
        print("Animal is eating...")
    }
    var description: String { // read-only property
        return "This is an animal named: \(self.name)"
    }
}

class Cat: Animal { // : inheritance
    var canClimb: Bool = true // extend property
    func sayMeomeo() { //extend method
        print("I am saying meo meo")
    }
    
    override func eat() { //override method
        super.eat() //super.func
        print("Cat is eating ...")
    }
    
    override var description: String { //override property
        return "Cat's name is: \(super.description), can climb = \(self.canClimb)"
    }
}
let animal = Animal() //instantiate
var myCat = Cat()
myCat.name = "Mina" //assign
myCat.eat()
print(myCat.description)

//myCat.description = "This is meomeo"


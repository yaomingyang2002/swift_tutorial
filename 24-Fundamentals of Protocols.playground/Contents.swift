//Protocols in Swift 4
import UIKit

// Protocols have properties and methods

protocol DetailInformationProtocol {
    //    Property Requirements
    var fullName: String { get }
    //Method Requirements
    func showDetail() -> String
}

//Example of a class "conforms a protocol"
class User: DetailInformationProtocol {
    var firstName: String
    var lastName: String
    init(firstName: String, lastName: String) {
        self.firstName = firstName
        self.lastName = lastName
    }
    //must implement property and function in Protocol
    var fullName: String { //get
        return firstName + lastName
    }
    func showDetail() -> String {
        return "Firstname = \(firstName), lastName = \(lastName)"
    }
}
var mrYang = User(firstName: "ym", lastName: "Yang")


//Example of "Mutating Method Requirements"
protocol TogglableProtocol {
    //Mutating Method Requirements
    mutating func toggle()
}
enum Switcher: TogglableProtocol {
    case on, off
    mutating func toggle() {
        switch self {
            case .off:
                self = .on
            case .on:
                self = .off
        }
    }
}
var lightSwitch = Switcher.off
lightSwitch.toggle()
lightSwitch.toggle()


//Initializer Requirements
protocol InitializationProtocol {
    init(name: String)
}
class Animal {
    
}
//class Bird inherited from Animal and conforms protocol InitializationProtocol
class Bird: Animal,InitializationProtocol {
    var name: String
    required init(name: String) {
        self.name = name
    }
}
let aBird = Bird(name: "Kiki")















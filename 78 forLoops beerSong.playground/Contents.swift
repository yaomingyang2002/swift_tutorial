import UIKit

//for loop:

let arrayOfNum = [1, 5, 3, 6, 9, 10]
var sum = 0

for num in arrayOfNum {
    sum += num
}
print (sum)

for number in 1..<10 where number % 3 == 0 {
    print(number)
}

for anyName in 1...5 {
    print("anyName is \(anyName)")
}

//78 Using Loops in Your Program :

/*Lyrics of the song, 99 bottles of beer:
 
99 bottles of beer on the wall, 99 bottle of beer.
Take one down and pas it around, 98 bottles of beert on the wall.

98 bottles of beer on the wall, 98 bottle of beer.
Take one down and pas it around, 97 bottles of beert on the wall.
 
...
 
 1 bottles of beer on the wall, 1 bottle of beer.
 Take one down and pas it around, no more bottles of beert on the wall.
 
 no more bottles of beer on the wall, no more bottle of beer.
 Go to the store and buy some more, 99 bottles of beert on the wall.
*/

func beerSong(withThisManyBottles totalNumOfBottles : Int) -> String {// (outsideParam insideParam : typeOfParam)
    var lyrics : String = ""
    
    for num in (2...totalNumOfBottles).reversed(){
        let newLine : String = "\n\(num) bottles of beer on the wall, \(num) bottle of beer.\nTake one down and pas it around, \(num-1) bottles of beert on the wall.\n\n "
        lyrics += newLine
    }
    
    lyrics += "\n1 bottle of beer on the wall, 1 bottle of beer.\nTake one down and pas it around, no more bottles of beert on the wall.\n"
    lyrics += "\nno more bottles of beer on the wall, no more bottle of beer.\nGo to the store and buy some more, 99 bottles of beert on the wall.\n"
    
    return lyrics
}

print(beerSong(withThisManyBottles: 99))

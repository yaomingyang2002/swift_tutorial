//Enumerations in Swift 4

import UIKit

enum Level { //Enum nem (Level) should start with upper case
//    case low //a basic enumeration
//    case medium
//    case high
    case low,medium, high //another option, seperate case by comma
}

//var myLevel = Level.high //one way to get the item, like class dot notation
var myLevel:Level = .high  // another way like class inheritance and dot notation

switch myLevel {
    case .high: // note the dot and the colon
        print("It is high level")
    case .medium:
        print("It is medium level")
    case .low:
        print("It is low level")
    default:
        print("Not a level")
}


enum AppleDevice {
    case appleWatch(String, UIColor) // Enum with associated values
    case iphone(Float, UIColor)
}

//var myDevice: AppleDevice = .appleWatch("sport 38mm", UIColor.green)
var myDevice = AppleDevice.iphone(10.3, UIColor.gray)
switch myDevice {
    case let .appleWatch(name, color):
        print("my device is Apple Watch with name = \(name), color: \(color)")
    case let .iphone(iosVersion, color):
        print("my device is Iphone with ios version = \(iosVersion), color: \(color)")
    default:
        print("Not apple watch or iphone !")
}


enum AnimalIcons: Character {
    case ant = "🐜"
    case optopus = "🦑"
    case pig = "🐖"
}
print("optopus icon is : \(AnimalIcons.optopus.rawValue)")

enum Direction:Int {
    case up = 1
    case down = 2
    case left = 3
    case right = 4
}

print("Raw value of Left is : \(Direction.left.rawValue)")









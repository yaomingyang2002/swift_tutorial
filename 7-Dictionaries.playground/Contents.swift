//: Playground - noun: a place where people can play
//dictionary: key:value pairs
import UIKit

var user:[String: String] = ["name": "yaoming", "email": "yaoming@example.com"]
var myNumbers:[Int: String] = [10: "ten", 123: "one hundred and twelve"]
//print(myNumbers[124])

print("user dictionary has \(user.count) keys")
print(user["email"]!)

//modify/update value
user["email"]="ym@gmail.com"
user.updateValue("ym12@gmail.com", forKey: "email") //return old value
print(user["email"]!)

if let oldEmail = user.updateValue("ym1234@gmail.com", forKey: "email") {
    print("old email = \(oldEmail)")
}
print("new email is: \(user["email"]!)")

//user = [:]
if user.isEmpty {
    print("user dictionary is empty")
} else {
    print("user dictionary is not empty")
}

print(user["email"]!)

for (myKey, myValue) in user { // for..in loop iterating dict
    print("key = \(myKey), value = \(myValue)")
}

for eachKey in user.keys {  //dict.keys
    print("each key = \(eachKey)")
}
for eachValue in user.values { //dict.values
    print("each value = \(eachValue)")
}

let allKeys = [String](user.keys)
print("all keys = \(allKeys)")

let allValues = [String](user.values)
print("all values = \(allValues)")






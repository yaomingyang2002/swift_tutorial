//: Playground - noun: a place where people can play
//flow control

import UIKit

for _ in 0..<5 { //for .. in loop
//    print("abc = \(abc)")
    print("hello !")
}
//for index in stride(from: 1, to: 12, by: 0.01) {
//    print("index =\(index)")
//}

var startValue = 0

while startValue < 5 {
    print("startValue = \(startValue)")
    startValue = startValue + 1
}
/*
 // repeat ... while , similar do...while
 repeat {
     print("startValue = \(startValue)")
     startValue = startValue + 1
 } while startValue < 100
 */


 var yourPoint = -2
 var estimation:String
 
 switch yourPoint {
     case 0..<2: // case can be a range
        estimation = "very bad"
     case 2..<5:
        estimation = "under average"
     case 5..<7:
        estimation = "average"
     case 7..<9:
        estimation = "Good"
     case 9...10:
        estimation = "Exellent"
        fallthrough // run through to default, not break!
     default:
        estimation = "Not a valid value"
 }
 
 print("estimation = \(estimation)")
 

let starWarCharacter = "ym yang"
switch starWarCharacter {
    case "Yoda", "Obiwan-Kenoby", "Mace Windu":
        print("\(starWarCharacter) is a Jedi")
    case "Darth Sidious", "Anakin Skywalker", "Darth Maul":
        print("\(starWarCharacter) is in Dark Side of the Force")
    case "ym yang":
        print("\(starWarCharacter) is a freelancer")
//        break //break the switch
        fallthrough   // run through to default
    default:
        print("\(starWarCharacter) is not a Star War's character")
}





















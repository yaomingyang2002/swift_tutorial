//Classses and structures in Swift 4

import UIKit

struct Shape { //structure is similar to class with properties only
    var width:Float = 0.0
    var height = 0.0
}
//var aShape = Shape() //instantiate or initialize a structure
var aShape = Shape(width: 120.0, height: 130.0)
//aShape.width = 102 //assign a new value
var shape2 = aShape //shape2, aShape use different memory location, are different!
print("shape's width = \(aShape.width)") //dot notation for properties
//print("shape's height = \(aShape.height)")
shape2.width = 1000
print("shape2's width = \(shape2.width)")


class User {
    var name = ""
    var universityName:String? //? optional
    var myBookShape = Shape(width:297,height: 210) //initialize from struct
}
var user1 = User()
var user2 = user1 //user2, user1 use same memory location =same, diff from struct!
var user3 = User()

if (user2 === user1) {//user2, user1 use same memory location =same
    print("user2 is identical to user1")
}
if (user3 === user1) {
    print("user3 is identical to user1")
} else {
    print("user3 is in different memory location with user1")
}
print("myBook height = \(user1.myBookShape.height)")
print("myBook width = \(user1.myBookShape.width)")



//: Playground - noun: a place where people can play

import UIKit

let welcome = """
  This is my tutorial channel. //add space
"How long have you done this ?". "4 months"
This is a 3 quotes \"\"\"

"""
//start and end tri double qouate for multi-line string



let aBlankString = String() //create a blank string
print(aBlankString)

if (aBlankString.isEmpty == true) { //String.isEmpty
    print("It is empty")
}

var aMutableString = "🦑This is a man."
aMutableString += "This is a woman" //+= to concatenate

//for eachCharacter in aMutableString {
//    print("character:           \(eachCharacter)")
//}

let dollarMark: Character = "$"
aMutableString.append(dollarMark)
print(aMutableString)

let width = 500
let squareArea = "The square area is \(width * width)" //string interporation
print(squareArea)

let ampersandMark = "\u{2602}" //search for unicode, \u{unicode} interporation
print(ampersandMark)

var welcomeString = "Hello World!"
print(welcomeString[welcomeString.index(welcomeString.startIndex, offsetBy: 4)]) //use offsetBy: to get individual character
/*
for index in welcomeString.indices {
    print("eachCharacter = \(welcomeString[index])")
}
*/

//welcomeString.insert("A", at: welcomeString.endIndex)
//String.insert(contentsOf:"char", at: posi)
welcomeString.insert(contentsOf: "AAAA", at: welcomeString.index(before: welcomeString.endIndex)) //before: String.endIndex is the final index of the string
print(welcomeString)

//Substring:
let someString = "I am a Jedi"
let indexFrom = someString.startIndex
let indexTo = someString.index(someString.startIndex, offsetBy: 5)
let subString1 = someString[indexFrom...indexTo] //subString
let subString2 = String(subString1) //clone string, not casting

let myLove1 = "I love you"
let myLove2 = "I love you"

if myLove1 == myLove2 {
    print("Content are equal")
}

let stringWithPrefix = "mr Hoang makes this video"
stringWithPrefix.hasPrefix("mr Hoang1") //.hasPrefix()
stringWithPrefix.hasPrefix("mr Hoang")

let stringWithPostfix = "mr Hoang likes StAr War"
stringWithPostfix.hasSuffix("Star War") //.hasSuffix()
stringWithPostfix.uppercased().hasSuffix("STAR WAR") //.uppercased()











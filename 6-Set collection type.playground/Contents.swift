//: Playground - noun: a place where people can play
import UIKit
//Set collections: unordered list, unique

var starWarCharacters: Set<String> = ["Anakin Skywalker", "Luke", "Yoda"] //create set

if(starWarCharacters.isEmpty){ //.isEmpty
    print("No characters")
} else {
    print("There are \(starWarCharacters.count) characters") //.count
}
starWarCharacters.insert("Leia") //.insert(ele), note set is unordered
starWarCharacters.remove("Luke") // .remove(ele)
print(starWarCharacters)

if starWarCharacters.contains("Finn") { //.contains(ele)
    print("List contains Finn")
} else {
    print("List does not contain Finn")
}

for starWarCharacter in starWarCharacters { //for .. in loop to iterating set
    print("starWarCharacter = \(starWarCharacter)")
}

//Set methods:

let set1: Set = [1, 2,  3, 4, 5,  6,  7]
let set2: Set = [9, 10, 3, 4, 11, 12, 13] // .sorted()

//let resultSet = set1.intersection(set2).sorted() //set1.intersection(set2)
//let resultSet = set1.union(set2).sorted()         //set1.union(set2)
//let resultSet = set1.subtracting(set2).sorted()   //set1.subtracting(set2)
let resultSet = set1.symmetricDifference(set2).sorted() //set1.symmetricDifference(set2) =get the difference
print("resultSet = \(resultSet)")


let fruits:Set = ["🍎","🥝","🍆","🍇","🍒"]
let fruitAndDrinks:Set = ["🍎","🥝","🍆","🍇","🍒", "🍺","☕️","🍷"]
let animals: Set = ["🐇","🐊","🐴","🐓"]
fruitAndDrinks.isSuperset(of: fruits) //.isSuperset(of: )
fruits.isSubset(of: fruitAndDrinks)     //.isSubset(of: )

fruits.isDisjoint(with: animals)  //.isDisjoint(with: ) =not overlap













//: Playground - noun: a place where people can play

import UIKit

var someDoubles = [Double]() //create a empty array, double data type
someDoubles.append(100.02) // array.append(ele)
someDoubles.append(3.2)
print("number of elements = \(someDoubles.count)") //array.count

var tenIntegers = Array(repeating: 5, count: 10) //create an array of 10 integer, value =5
var twoIntegers = Array(repeating: 9, count: 2) //Array()

print(tenIntegers)
print(twoIntegers)

var result1 = tenIntegers + twoIntegers //Adding 2 arrays
print(result1)

var smartPhones = ["iphone", "samsum galaxy", "htc one"]
smartPhones += ["nokia", "blackberry"]  // += assignment
print(smartPhones)
print(smartPhones[1...2]) //slicing
//smartPhones.remove(at: 1) // .remove(at: pos)
//print(smartPhones)

for smartPhone in smartPhones { // for loop iterating an array
    print("each smartphone = \(smartPhone)")
}

for (index, value) in smartPhones.enumerated() { //unpacking tuple, .enumerated()
    print("index = \(index), value = \(value)")
}



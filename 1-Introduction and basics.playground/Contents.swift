//: Playground - noun: a place where people can play

import UIKit

// let =const, var =variable
let pi = 3.1416
//var x = 102.3

var name = "Hoang", age = 20, email = "sunlight4d@gmail.com" // define maultple var
print("name = \(name), age = \(age)")   //print formate
/*
 multpile line commnet
 */

var x, y, z: Double
let 🐜🌿 = "I love ant - 🐜" //use emoji or symbols
let 🦜 = "I like bird" //to get emoji: Edit->emoji & symbols -> search animal

let oneBillion = 1_000_000_000; print("one billion = \(oneBillion)") // multiple line code need semicolon

let aNumber:UInt64 = 100 // Type annotation :
let anotherNumber:UInt8 = 10
let result1 = aNumber + UInt64(anotherNumber) //type conversion
print("result 1 = \(result1)")


typealias BigNumber = Double
let numberOfAnts:BigNumber = 1_000_000  // 1000000.0
print("numberOfAnts 1 = \(numberOfAnts)")

//optional ?
//var numberOfMessages: Int = 10
//numberOfMessages = nil //not allowed!

var numberOfMessages: Int? = 10 // "?" =optional -> can be nil
numberOfMessages = nil
print ("numberOfMessages = \(numberOfMessages)")

var ageNumber = 10;
assert(ageNumber >= 0, "Age must be larger than or equal 0") // assert(<#T##condition: Bool##Bool#>, <#T##message: String##String#>), //assert when "false, throw error

precondition(ageNumber >= 0, "Age must be larger than or equal 0")
var x1 = 100



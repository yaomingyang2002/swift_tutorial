import UIKit
/*
func bmiCalculator (weight : Float, height : Float)-> String {
    
    let BMI = weight  / (height * height) //height * height = height ^2 = pow(height, 2)
    
    let shortedBMI = String(format: "%.2f", BMI)
    
    var interpretation = "Your weight is: \(weight) kg\n     height is: \(height) m\n     Body Mass Index is: \(shortedBMI).\n \n"
    
    if BMI > 25 {
        interpretation = "\(interpretation)You are overweight!"
    }
    else if BMI > 18.5 {
        interpretation = "\(interpretation)You are within normal weight range."
    }
    else {
        interpretation = "\(interpretation)You are underweight!"
    }
    
    return interpretation
}

print(bmiCalculator(weight: 95, height: 1.80))
 */


func bmiCalcImperialUnits (weightInPounds : Double, heightInFeet : Double, remainderInches : Double)-> String {
// 1 foot = 12 inches
// 1 inch = 0.0254 metres
// 1 pound = 0.45359237 kilograms
    let weightInKg = weightInPounds * 0.45359237
    let totalInches = heightInFeet * 12 + remainderInches
    let heightInMetres = totalInches * 0.0254
    let BMI = weightInKg / (heightInMetres * heightInMetres)
    
    let shortedWeightInKg = String(format: "%.2f", weightInKg)
    let shortedHeightInMetres = String(format: "%.2f", heightInMetres)
    let shortedBMI = String(format: "%.2f", BMI)
    
    var interpretation = "Your weight is: \(shortedWeightInKg) kg\n     height is: \(shortedHeightInMetres) m\n     Body Mass Index is: \(shortedBMI).\n \n"
    
    if BMI > 25 {
        interpretation = "\(interpretation)You are overweight!"
    }
    else if BMI > 18.5 {
        interpretation = "\(interpretation)You are within normal weight range."
    }
    else {
        interpretation = "\(interpretation)You are underweight!"
    }
    
    return interpretation
 
    
}

print(bmiCalcImperialUnits (weightInPounds : 140, heightInFeet : 6, remainderInches : 0))


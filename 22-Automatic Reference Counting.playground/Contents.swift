//Automatic Reference Counting - ARC in Swift 4

import UIKit


class Customer {
    let name: String
    init(name: String) {
        self.name = name
        print("\(self.name) is being initialized")
    }
    deinit {
        print("\(self.name) is being deinitialized")
    }
    var house: House?
}


var customer1: Customer?        // strong reference to nil
var customer2: Customer?        // strong reference to nil
var customer3: Customer?        // strong reference to nil
customer1 = Customer(name: "ym Yang") //customer instance
customer2 = customer1               //pointers to customer instance
customer3 = customer1               //pointers to customer instance
//customer1 = nil     // deinit customer1 object
//customer2 = nil     // deinit customer2
//customer3 = nil     // deinit customer3


class House {
    var address: String
    init(address: String) {
        self.address = address
    }
    weak var author: Customer? // weak reference
    deinit {
        print(" House at: \(address) is being deinitialized")
    }
}

var mrYang:Customer?
var hisHouse:House?
mrYang = Customer(name: "ym Yang")
hisHouse = House(address: "45 sebastien, Montreal, Canada")
mrYang!.house = hisHouse
hisHouse!.author = mrYang
mrYang = nil
hisHouse = nil


class User {
    let name: String
    var card: CreditCard?
    init(name: String) {
        self.name = name
    }
    deinit {
        print("User: \(name) is being deinitialized")
    }
}

class CreditCard {
    let number: UInt64
    unowned var user: User
    init(number: UInt64, user: User) {
        self.number = number
        self.user = user
    }
    deinit { print("Card #\(number) is being deinitialized") }
}

var mrAlex:User?
var creditCard:CreditCard?

mrAlex = User(name: "Alex")
creditCard = CreditCard(number: 1234_5678_9012_3456, user:mrAlex!) //initialized
mrAlex!.card = creditCard
mrAlex = nil    //deinitialized
creditCard = nil




//Subscript in Swift 4
//subscript is a function inside struct, can be called by using []

import UIKit


struct MultiplicationTable {
    let multiplier: Int
    subscript(index: Int) -> Int { // read-only subscript
        return multiplier * index
    }
}

let mt = MultiplicationTable(multiplier: 4) //initialize a struct
mt[2] //8, calling subscript using [], like array

for i in 1...10 {
    print("\(i) multiplies by 4 is : \(mt[i])") // calling subscript using [], like array
}

//mt[2] = 100, //subscript is get-only and cannot reassigned

//subscript with getter and setter
struct AnimalMatrix {
    var animals = [
        ["🐜", "🐙", "🐊", "🐎", "🐖"],
        ["🐢", "🐍", "🦋", "🦀", "🐿"],
        ["🐌", "🐚", "🐳", "🐒"]
    ];
    subscript(row: Int, column: Int) -> String { //subscript with getter and setter
        get {
            assert(animals[row][column] != nil, "No animal found") //if assert false, show the alert and break
            return animals[row][column]
        }
        set {
            assert(animals[row][column] != nil, "Cannot set animal value because it is out of range")
            animals[row][column] = newValue // remember to use this name!
        }
    }
}

var am = AnimalMatrix() // initialize a struct
print(am[2, 3]) // note start from [0,0]
am[0, 0] = "xyz" //set

print("am = \(am)")





import UIKit

/*

Fibonaaci numbers
 0, 1, 1, 2, 3, 5, 8, ...

 
 */

func fibonaaci(until num: Int)->Array<Int> {
    var a : Int = 0
    var b : Int = 1
    var fibonaaciArray  = [a, b]
    
    for _ in 0...num {
        let c = a + b
        fibonaaciArray.append(c)
        a = b
        b = c
    }
    return fibonaaciArray
}

print(fibonaaci(until: 10))

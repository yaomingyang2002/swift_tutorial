//Properties in Swift 4

import UIKit

struct Patient { //basic structure without stored property values
    var name: String
    let yearOfBirth: Int
}
var aPatient = Patient(name: "Alex",yearOfBirth: 1992) //initializ a structure
aPatient.name = "Anderson" //re-assign a variable property
//aPatient.yearOfBirth = 1993 //cannot reassign a constant (let) property


class MovieManager {
    lazy var getOneMovieName:String = { //lazy loading, only run when called
        print("do hard work...")
        return "Star War"
    }() //a method
}
let manager = MovieManager() //initialize an instantce of class
print("get 1 movie : \(manager.getOneMovieName)") //dot
manager


struct Calculation {
    var x: Double
    var squaredX: Double { //property with getter and setter
        get {   //getter
            return x * x
        }
        set {   //setter
            print("new value = \(newValue)")    // remember to use this name
            x = sqrt(newValue)
        }
    }
    var cubeX:Double { //read-only property without getter and setter
        return x * x * x
    }
    var step:Int = 0 {
        willSet(newStep) {
            print("1. Before set value : \(newStep)")
        }
        didSet {
            print("2. After set value: \(step), old value: \(oldValue)")
        }
    }
}
var calculation = Calculation(x: 10, step: 3) // initialize a construct with properties
print("squared = \(calculation.squaredX)")  //call the getter of the construct
calculation.squaredX = 256.0        // call the setter of the construct's property
print("x now = \(calculation.x)")   //call the getter again from squaredX ->x
calculation.cubeX //can call getter of property without setter
//calculation.cubeX = 1000 //cannot call setter of property without setter
calculation.step = 123 //willSet(){}, didSet{}


class Computation {
    static var title = "This is Calculation class" //static property only for setOf values, .. etc
    static let pi: Float = 3.1416
    class var squaredPi: Float { //Calculated static property
        return pi * pi
    }
}
print("title = \(Computation.title)")
Computation.pi
Computation.squaredPi















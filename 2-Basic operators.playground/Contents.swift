//: Playground - noun: a place where people can play
//Basic operators

import UIKit

let isHero:Bool = true
let check = !isHero //! unary operator

let (width, height) = (800, 600) // tuple unpacking. "=" assign operator

let x = "hello " + "world" + "!" //"+": concatenate
let y = 3 % 2       //+ ,-, *, / %, arythmatic operator

2>1     // comparison operators: >, < , =, >=, >=, !=
(3, "cat") == (3, "cat") //compare two tuples

let isHD = (width, height) > (1280, 720) ? "is HD" : "not HD" //Ternary operator
let age:Int? = nil
let myAge = age ?? 18 // if age == nil, then myAge = 18

for index in 1..<5 { // 1...5
    print("index = \(index)")
}

if (5 < 1 || 3 < 2) {   // or ||, and &&
    print("true !")
} else {
    print("false")
}

let range = ...7 //range
range.contains(6)
range.contains(8)


let animals = ["cat", "dog", "tiger", "dinosaur"]

for animal in animals[...2] {
    print("animal = \(animal)")
}



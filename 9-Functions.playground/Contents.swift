//Functions in Swift 4

import UIKit
//func funcName (params:types) -> returnType { return ...}
func sum2Numbers(a: Int, b: Int) -> Int {
//    let c = a + b
    return a + b
}
print(sum2Numbers(a: 2, b: 4))

//func funcName (no param) { with no return ...} // like void in C
func sayHello() {
    print("Hello, how are you ?")
}
sayHello()

func calculateSumAndMultiply(a: Double, b: Double) -> (sum: Double, multiply: Double)? { //optional? similar to **kwarg
    let result = (a + b, a * b)
    return result
}
//calculateSumAndMultiply(a: 2, b: 3))
calculateSumAndMultiply(a: 4, b: 5) // kwarg

if let result1 = calculateSumAndMultiply(a: 10, b: 20) {
    print("result1 = \(result1)")
} else {
    print("result is nil")
}

func multiply2Numbers(_ a: Float, _ b: Float) -> Float{ // _ mean func without arg labels, or position arg, similar to *arg
    return a * b
}
multiply2Numbers(100, 30.1) //positional args
//print("Multiply without parameter names : \(multiply2Numbers(120, 30))")


func getRectangleArea(width: Double, height: Double = 100) -> Double { //default param
    return width * height
}
//print("Area of the rectangle is: \(getRectangleArea(width: 10.0, height: 20.0))")
//print("Area of the rectangle is: \(getRectangleArea(width: 10.0))")

func sumMultipleNumbers(_ numbers: Double...) -> Double { //Variadic params = *args
    var total: Double = 0.0
    for number in numbers {
        total = total + number
    }
    return total
}
print("Total numbers = \(sumMultipleNumbers(1, 2))")

//inout arg.
var aString = "I am a man"
print("String before: \(aString)")
func inoutFunction(_ a: inout String) {
    //inout = can be changed after running function,similar to .map(arg)
    a = "I am a hero"
}
inoutFunction(&aString) // &: pointor
print("String after: \(aString)")

// nested functions
func showGreeting() {
    func showHello() {
        print("Hello everybody !")
    }
    func showAQuestion() {
        print("How about your work ?")
    }
    showHello()
    showAQuestion()
}

showGreeting()














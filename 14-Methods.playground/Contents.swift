//Methods in Swift 4


import UIKit

class Point {
    var x:CGFloat = 0.0
    var y:CGFloat = 0.0
    var color: UIColor = UIColor.green
    
    func moveTo(x: CGFloat, y: CGFloat, color: UIColor) { // instance method, constructor, initiator
        self.x = x
        self.y = y
        self.color = color
    }
    
    func toString() -> String { // a func
        return "x = \(self.x), y = \(self.y), color = \(self.color.description)"
    }
    
    func moveAndCreateNewPoint(x: CGFloat, y: CGFloat, color: UIColor) -> Point { // move to new point
        let newPoint = Point()
        newPoint.moveTo(x: x, y: y, color: color)
        return newPoint
    }
}

var myPoint = Point() // create an instance of Point
myPoint.moveTo(x: 10, y: 20, color: UIColor.red)
print(myPoint.toString())
var newPoint = myPoint.moveAndCreateNewPoint(x: 200, y: 300, color: UIColor.yellow)
if myPoint !== newPoint {
    print("myPoint and newPoint are different")
}


enum Compass {
    case east, west, south, north
    mutating func moveToNext() { //a mutating function
        switch self {
            case .east:
                self = .west //change self for mutating function
            case .west:
                self = .south //change self for mutating function
            case .south:
                self = .north //change self for mutating function
            case .north:
                self = .east //change self for mutating function
        }
    }
}

var compass1 = Compass.east
compass1.moveToNext() //from east to west
print(compass1) //west
compass1.moveToNext() //from west to south
print(compass1) //south


class Calculator {
    static let pi:Float = 3.1416
    static func sum2Numbers(a: Int, b: Int) -> Int { //static func
        return a + b
    }
    class func circleArea(radius: Float) -> Float { //class func, Type method, can be inherited
        return pi * radius * radius
    }
}
print("sum 5 + 2 = \(Calculator.sum2Numbers(a: 5, b: 2))") // call static/type method using class name without instanciated
print("circle area = \(Calculator.circleArea(radius: 20))")














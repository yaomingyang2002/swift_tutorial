import UIKit

var str = "Hello, playground"

//one line comment
/*
 multiple
 line comments
 
 */

//print statement

var monsterHealth = 19
print(monsterHealth)
print("hello world")

/*
 Types:
 ---
 Int        1, 25, 589, 30_000
 Float      1.6, 6.89, 4.6789,
 Double     3.14159 3.1415925359
 Bool       true, false
 String     "Angela", "Philipp"
 ---
 
 Variables:
 ---
 let iAmAConstant : Int = 42 // let = constent
 var iAmAVariable : Int = 23
 later... iAmAVariable = 46
 var inferredVariable = "I'm a string"
 var optionalString:String? = nil
 ---
 
 Strings:
 ---
 var combi = "\(string1) + \(string2)"
 let numberString = "2"
 var integer =numberString.toInt
 ---
 
 Arrays + Dict:
 ---
 let one = "Uno"
 let two = "Dos"
 var array : [String] = ["one", "two"]
 
 array.append("Tres")
 print("two = \(array[1]")
 
 var dict : Dictionary [String: Int] = ["One": 1, "Two": 2]
 dict["Two"] = "Dos"
 dict["One"] = nil //=delete for (string, number) in dict{ }
 ---
 
Classes:
-----
class myClass:someSuperClass {
 var myProperty:Int?
 
 override init() {
    myProperty = 12
 
 }
 
 //more methods here
 
}

----
*/
/*
 methods/functions:

func myMethod() -> Bool {
 return true
}

func methodWithParam (a:Int, b:Int) {
 a+b
    
}
 
 */

//func getMilk(howManyMilkCartons : Int){
//    print("buy \(howManyMilkCartons) cartons of milk")
//    let priceToPay = howManyMilkCartons * 2
//    print(priceToPay)
//}
//
//getMilk(howManyMilkCartons : 4)

// with -> return output function
func getMilk(howManyMilkCartons : Int, howMuchMoneyRobotHave : Int) -> Int{
    print("buy \(howManyMilkCartons) cartons of milk")
    let priceToPay = howManyMilkCartons * 2
    print("pay $\(priceToPay)")
    let change = howMuchMoneyRobotHave - priceToPay
    return change
}

var amountOfChange = getMilk(howManyMilkCartons : 4, howMuchMoneyRobotHave: 20)
print("Hello master, here's your $\(amountOfChange) change")
 
/*
 To generate random number: either use
 ---
 arc4random_uniform()
 Int.random(in: startNum ... stopNum)
 ---
*/


/*
If conditional:
-----
if someCondition == true {
    //do x
} else {
    //do y
}
---
*/

/*
For Loops:
 ---
for var i = 0 ; i < 4 ; i++ {
    //do smthin
 
}
 
for i in 0...4 {
    //do something else
 
}

for i in 0..<4 {
    //do another thing
}
-----
 
Switch:
 -----
switch someVariable {
 case 1: "Hello"
 case 2: "Good Bye"
 default: "Nothing"
}
-----

 */


//Optional Chaining in Swift 4

import UIKit

/*
class User {
    var name: String
    var readingBook: Book?
    init(name: String) {
        self.name = name
    }
}
 
class Book {
    var numberOfPages = 1000
}
 
var mrYang = User(name: "ym Yang")
mrYang.readingBook = Book()
 
if let numberOfPages = mrYang.readingBook?.numberOfPages {
    print("Book of \(mrYang.name) has \(numberOfPages) pages.")
} else  {
    print("Failed to get number of pages")
}
 */

class Address {
    var buildingName: String?
    var buildingNumber: String?
    var street: String?
    func detailAddress() -> String? {
        if let buildingName = buildingName, let buildingNumber = buildingNumber, let street = street {
            return "\(buildingName) \(buildingNumber) \(street)"
        } else {
            return nil
        }
    }
}

let address = Address()
address.buildingName = "Sunrise Building"
address.buildingNumber = "A1234"
address.street = "Acacia Road"

if let detailAddress = address.detailAddress() {
    print(detailAddress)
} else {
    print("Cannot get detail address")
}
 
/*
if address.detailAddress() != nil {
    print(address.detailAddress())
} else {
    print("Cannot get detail address")
}
*/

//array
var testScores = [
                    "Hoang": [60, 70, 80],
                    "Alex": [79, 94, 81]
                ]
//testScores["Hoang"]?[0] = 99
//testScores["Alex"]?[0] += 1
testScores["Hoang1234"]?[0] = 99
print(testScores)

